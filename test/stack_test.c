#include <check.h>
#include "../src/include/stack.h"

Stack *s = NULL;

void setup(void)
{
    s = createStack(3);
}
void teardown(void)
{
    freeStack(s);
}

START_TEST(push_test)
{

    push(s, 'a');
    ck_assert_int_eq(pop(s), 'a');
    push(s, 'b');
    push(s, 'c');
    push(s, 'd');
    ck_assert(pop(s) == 'd');
    ck_assert_int_eq(pop(s), 'c');
    ck_assert_int_eq(pop(s), 'b');
}
END_TEST

START_TEST(pop_test)
{
    Stack *s = createStack(3);
    ck_assert_int_eq(pop(s), INT_MIN);

    push(s, 'b');
    push(s, 'c');
    ck_assert_int_eq(pop(s), 'c');
    ck_assert_int_eq(pop(s), 'b');
}
END_TEST

START_TEST(peek_test)
{
    Stack *s = createStack(3);
    push(s, 'c');
    push(s, 'd');

    ck_assert_int_eq(peek(s), 'd');
    ck_assert_int_eq(peek(s), 'd');
}
END_TEST

START_TEST(isFull_test)
{
    Stack *s = createStack(3);
    push(s, 'a');
    push(s, 'b');
    push(s, 'c');

    ck_assert(isFull(s) == true);
}
END_TEST

START_TEST(isEmpty_test)
{
    Stack *s = createStack(3);
    ck_assert(isEmpty(s) == true);

    push(s, 'a');
    push(s, 'b');
    push(s, 'c');

    ck_assert(isEmpty(s) != true);
}
END_TEST

Suite *stack_suite()
{

    Suite *s;

    s = suite_create("stack");

    TCase *push_pop = tcase_create("push_pop_test");

    TCase *peek = tcase_create("peek_test");

    TCase *full = tcase_create("full_test");

    TCase *empty = tcase_create("empty_test");

    tcase_add_unchecked_fixture(push_pop, setup, teardown);
    tcase_add_test(push_pop, push_test);
    tcase_add_test(push_pop, pop_test);

    tcase_add_test(peek, peek_test);

    tcase_add_test(full, isFull_test);

    tcase_add_test(empty, isEmpty_test);

    suite_add_tcase(s, push_pop);
    suite_add_tcase(s, peek);
    suite_add_tcase(s, full);
    suite_add_tcase(s, empty);

    return s;
}
