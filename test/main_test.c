#include "postfix_test.c"
#include "stack_test.c"

int main()
{

    SRunner *sr_post = srunner_create(post_suite());
    SRunner *sr_stack = srunner_create(stack_suite());

    //setenv("CK_RUN_SUITE","stack",0);
    srunner_set_log(sr_stack,"../log/stack.log");
    srunner_set_xml(sr_post,"../log/post.xml");
    srunner_run_all(sr_stack, CK_NORMAL);
    srunner_run_all(sr_post, CK_NORMAL);
      
    int number_failed = srunner_ntests_failed(sr_stack);
    srunner_free(sr_stack);

    if (number_failed != 0)
        return EXIT_FAILURE;

    number_failed = srunner_ntests_failed(sr_stack);
    srunner_free(sr_post);

    if (number_failed != 0)
        return EXIT_FAILURE;

    return EXIT_SUCCESS;
}
