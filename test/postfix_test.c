#include <check.h>
#include "../src/include/postfix.h"

static const char *exp[] = {"(a+(b*c))", "((a+b)*(z+x))", "((a+t)*((b+(a+c))^(c+d)))"};
static const char *ans[] = {"abc*+", "ab+zx+*", "at+bac++cd+^*"};

START_TEST(infix_to_postfix_test)
{
    ck_assert_str_eq(infix_to_postfix(exp[_i]), ans[_i]);//**

}
END_TEST

START_TEST(operand_test)
{

    ck_assert(is_operand('a') == true);
    ck_assert(is_operand('F') != true);
    ck_assert(is_operand('1') != true);
    ck_assert(is_operand('z') == true);
}
END_TEST

START_TEST(operator_test)
{

    ck_assert(is_operator('*') == true);
    ck_assert(is_operator('-') == true);
    ck_assert(is_operator('(') != true);
    ck_assert(is_operator('&') == false);
}
END_TEST

Suite *post_suite()
{

    Suite *s;

    s = suite_create("postfix_to_infix");

    TCase *op = tcase_create("operator_test");

    TCase *ope = tcase_create("operand_test");

    TCase *onp = tcase_create("infix_to_postfix_test");

    tcase_add_test(op, operator_test);

    tcase_add_test(ope, operand_test);

    tcase_add_loop_test(onp, infix_to_postfix_test,0,3);//**

    tcase_set_timeout(onp, 1);

    suite_add_tcase(s, op);
    suite_add_tcase(s, ope);
    suite_add_tcase(s, onp);

    return s;
}
