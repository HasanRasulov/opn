#include "include/postfix.h"


_Bool is_operand(char c){
    return c>='a' && c<='z';
}

_Bool is_operator(char c){
    return c=='+' || c=='-' || c=='*' || c=='/' || c=='^';
}


char*  infix_to_postfix(const char* in){

    Stack* s = createStack(200);

    size_t i,j=0;

    char* res =(char*) calloc(sizeof(char),400);

    for(i=0;in[i];i++){

        if(in[i]=='('||is_operator(in[i])){
            push(s,in[i]);
        }else if(is_operand(in[i])){
            res[j++]=in[i];
        }
        else if(in[i]==')'){
            res[j++]=peek(s);
            pop(s);
            pop(s);
        }
    }

    freeStack(s);
    return res;
}




