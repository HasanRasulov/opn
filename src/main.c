#include "include/postfix.h"



int main()
{

   int t;

   scanf("%d",&t);

   char in[400];

   while(t--){

    memset(in,'\0',400);

    scanf("%s",in);

    char* res = infix_to_postfix(in);

    printf("%s\n",res);
 
    free(res);
}
 
    return 0;
}
