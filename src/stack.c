#include "include/stack.h"


struct Stack* createStack(unsigned capacity)
{
    struct Stack* stack = (struct Stack*)malloc(sizeof(struct Stack));
    stack->capacity = capacity;
    stack->top = -1;
    stack->array = (char*)calloc(stack->capacity , sizeof(char));
    return stack;
}

_Bool isFull(struct Stack* stack)
{
    return stack->top == stack->capacity - 1;
}


_Bool isEmpty(struct Stack* stack)
{
    return stack->top == -1;
}


void push(struct Stack* stack, char item)
{
    if (isFull(stack))
        return;
    stack->array[++stack->top] = item;
}


int pop(struct Stack* stack)
{
    if (isEmpty(stack))
        return INT_MIN;
    return stack->array[stack->top--];
}


int peek(struct Stack* stack)
{
    if (isEmpty(stack))
        return INT_MIN;
    return stack->array[stack->top];
}

void freeStack(Stack* stack){
    free(stack->array);
    free(stack);
}