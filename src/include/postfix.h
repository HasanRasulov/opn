#ifndef POSTFIX_H
#define POSTFIX_H

#include "stack.h" 

_Bool is_operand(char c);

_Bool is_operator(char c);

char*  infix_to_postfix(const char* in);

#endif
