#ifndef STACK_H
#define STACK_H


#include<stdio.h>
#include<stdbool.h>
#include<limits.h>
#include<stdlib.h>
#include<string.h>


typedef struct Stack {
    int top;
    int capacity;
    char* array;
} Stack;



struct Stack* createStack(unsigned capacity);

_Bool isFull(struct Stack* stack);

_Bool isEmpty(struct Stack* stack);

void push(struct Stack* stack, char item);

int pop(struct Stack* stack);

int peek(struct Stack* stack);

void freeStack(Stack* stack);

#endif